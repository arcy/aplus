<?php

namespace App\Http\Controllers\Admin\Auth;

use URL;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use App\Helper\Tree;
use App\Model\AdminRole;

class RoleController extends Controller{

	/* 规则列表 */
	public function lists(Request $request){
		/* 实例化模型 */
		$model  = new AdminRole();
		
		$list = $model->page(15);

		// var_dump(session()->all());

		$data = ['lists'=>$list];
		return view('admin.auth.role_list',$data);
	}

	/* 添加规则 */
	public function add(Request $request){

		$insert = [];

		if($request->isMethod('get')){
			$rule_list = \App\Model\AdminRule::where('status',1)->get(['title as text','rule_id','parent_id'])->toArray();

			$tree = new Tree();
			$tree->init($rule_list)->set_id_key('rule_id')->set_pid_key('parent_id');
			$rule_list = $tree->get_tree_array('nodes');

			$rule_list = json_encode($rule_list);
			
			$data = ['rule_list'=>$rule_list];
			return view('admin.auth.role_add' , $data);
		}

		//添加权限进数据库
		$name = $request->input('name');
		$remark = $request->input('remark');
		$rule_id = $request->input('rule_id');
		$status = $request->input('status');
		
		if(!between($name,1,16)){
			return $this->error('角色名字介乎1-16个字符！');
		}elseif (!is_empty($remark) && !between($remark,1,288)) {
			return $this->error('备注介乎1-288个字符！');
		}

		//过滤权限
		$rule_id = str_to_arr($rule_id);

		foreach ($rule_id as $key => $value) {
			if(!is_id($value)){
				unset($rule_id[$key]);
			}
		}

		$rule_id = implode(',', $rule_id);

		$status = ($status == 'on') ? 1 : 0;

		$insert = array_merge([
			'name' => $name,
			'remark' => $remark,
			'rule_id' => $rule_id,
			'status' => $status
		],$insert);

		if(adminRole::create($insert)){
			return $this->success('添加成功！' , URL::route('admin.auth.role.add'));
		}else{
			return $this->error('添加失败！');
		}

		
	}

	public function edit(Request $request){

		//判断ID是否合法
		$id = $request->input('id');
		if(!is_id($id)){
			return $this->error('角色ID非法！');
		}

		//判断ID是否存在
		if(adminRole::where('role_id',$id)->count() < 1){
			return $this->error('角色不存在！');
		}
		//获取角色数据
		$info = adminRole::find($id);

		//权限被禁用
		if($info->status == 0){
			return $this->error('角色已被禁用！');
		}

		if($request->isMethod('get')){

			//获取
			$rule_id = str_to_arr($info->rule_id);

			/* 树形菜单选择 */
			$rule_list = \App\Model\AdminRule::where('status',1)->get(['title as text','rule_id','parent_id'])->toArray();

			//预处理数据
			foreach ($rule_list as $key => $value) {
				if(in_array($value['rule_id'], $rule_id)) $rule_list[$key]['state']['checked'] = true;
			}

			$tree = new Tree();
			$tree->init($rule_list)->set_id_key('rule_id')->set_pid_key('parent_id');
			$rule_list = $tree->get_tree_array('nodes');

			$rule_list = json_encode($rule_list);
			
			$data = ['rule_list'=>$rule_list,'info'=>$info];
			return view('admin.auth.role_edit',$data);
		}

		//添加权限进数据库
		$name = $request->input('name');
		$remark = $request->input('remark');
		$rule_id = $request->input('rule_id');
		$status = $request->input('status');
		
		if(!between($name,1,16)){
			return $this->error('角色名字介乎1-16个字符！');
		}elseif (!is_empty($remark) && !between($remark,1,288)) {
			return $this->error('备注介乎1-288个字符！');
		}

		//过滤权限
		$rule_id = str_to_arr($rule_id);

		foreach ($rule_id as $key => $value) {
			if(!is_id($value)){
				unset($rule_id[$key]);
			}
		}

		$rule_id = implode(',', $rule_id);

		$status = ($status == 'on') ? 1 : 0;

		//整合数据
		$info->name = $name;
		$info->remark = $remark;
		$info->rule_id = $rule_id;
		$info->status = $status;

		if($info->save()){
			return $this->success('修改成功！' , URL::route('admin.auth.role.list'));
		}else{
			return $this->error('修改失败！');
		}

	}

	public function del(Request $request){
		$id = $request->input('id');

		if(!is_id($id)){
			return $this->error('角色ID非法！');
		}

		$adminRole = new adminRole();

		if($adminRole->where('role_id',$id)->delete()){
			return $this->success('删除成功！');
		}else{
			return $this->error('删除失败！');
		}
	}

	public function changeStatus(Request $request){
		$id = $request->input('id');
		$option = $request->input('option');

		if (!is_id($id)) {
			return $this->error('角色ID非法！');
		}elseif (is_empty($option)) {
			return $this->error('option不能为空！');
		}

		$adminRule = new \App\Model\AdminRole();
		$status = $adminRule->changeStatus($option,$id);

		if($status === false){
			return $this->error('更新失败！');
		}else{
			return $this->success('更新成功！' , null, ['status'=>$status]);
		}
	}
}