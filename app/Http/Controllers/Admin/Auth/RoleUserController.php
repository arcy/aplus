<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use App\Model\AdminRoleUser;

class RoleUserController extends Controller{

	/* 规则列表 */
	public function lists(Request $request){
		/* 实例化模型 */
		$model  = new AdminRole();
		
		$list = $model->page(15);

		$data = ['lists'=>$list];
		return view('admin.auth.role_list',$data);
	}

	
	public function del(Request $request){
		$id = $request->input('id');

		if(!is_id($id)){
			return $this->error('角色ID非法！');
		}

		if($adminRule->where('role_id',$id)->delete()){
			return $this->success('删除成功！');
		}else{
			return $this->error('删除失败！');
		}
	}
}