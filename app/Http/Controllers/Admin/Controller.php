<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as CommonController;
use View;

class Controller extends CommonController{

	public function __construct(){

		$this->middleware(function($request , $next){
			/* 定义全局模板变量 */
			if(session()->has('admin_user_info')) View::share('SELF', session()->get('admin_user_info'));

			/* 获取菜单ID并复制到session */
			$menu_id = $request->input('menu_id');
			if(is_id($menu_id)) $request->session()->put('admin_menu_id', $menu_id, 1000);

			return $next($request);
		});




		/* admin中间件用于提供GATE，判断用户权限 */
		$this->middleware('admin.auth' , ['except'=>['login','mkpsd','loginout']]);
	}


}