<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Admin;
use Route;
use URL;
use App\Helper\Tree;
class HomeController extends Controller{

	private $menu_loop_level = 0;

	/* 默认首页 */
	public function home(Request $request){

		$tree = new Tree();
		$list = Admin::userMenu();

		$tree->init($list)->set_id_key('rule_id')->set_pid_key('parent_id');
		$list = $tree->get_tree_array();
		$menu_str = $this->getMainMenu($list,2);

		$data = ['menu_str'=>$menu_str];
		return view('admin.home', $data);
	}

	/* 后台首页 */
	public function index(){

		// (new \App\Model\AdminUser())->getUserRuleId();
		Admin::userMenu();

		/* 获取系统参数 */
		//mysql 版本
		$mysql_version = DB::select("SELECT VERSION() AS version");

		$disk_space = [
			'free' => config('system.disk_free_space'),
			'total' => config('system.disk_total_space'),
		];

		$disk_free_precent = round($disk_space['free'] / $disk_space['total'] * 100 , 2);

		$info = [
			'操作系统' => config('system.php_os'),
			'运行环境' => config('system.run_env'),
			'PHP运行方式' => PHP_SAPI,
			'MYSQL版本' => $mysql_version[0]->version,
			'产品名称' => config('system.product_name'),
			'产品流水号' => config('system.product_num'),
			'上传附件限制' => config('system.upload_max_filesize'),
			'执行时间限制' => config('system.max_execution_time'),
			'服务器剩余空间'=>$disk_space['free'] . 'M',
			'服务器总容量' =>$disk_space['total'] . 'M',

		];


		$data = ['info'=>$info, 'disk'=>$disk_space, 'disk_precent'=>$disk_free_precent];

		return view('admin.index', $data);
	}

	/**
	 * 获取格式为
	 *<li>
	 *	<a href="#">text</a>
	 *  <ul class="nav">
	 *    <li><a href='#'>text</a></li>
	 * 	</ul>
	 *</li>
	 * @param  array   $list   需要菜单的列表
	 * @param  integer $level_at 生成菜单层级
	 * @param  integer $level 出事层级
	 * @return str           生成的菜单
	 */
	public function getMainMenu($list,$level_at = 0,$level=0){
		if(!is_array($list)) return '';
		$menu_list = '';

		if($level > 0){
			$menu_list .= '<ul class="nav">';
		}
		$level++;
		foreach ($list as $key => $value) {
			
			$menu_list .= '<li>';

			if($level_at > $level && isset($value['_child'])){
				$menu_list .= '<a href="#"><i class="'.$value['icon'].'"></i> <span class="nav-label">'.$value['title'].'</span><span class="fa arrow"></span></a>';
				$menu_list .= $this->getMainMenu($value['_child'],$level_at,$level);
			}else{

				//判断是否存在路由
				if(Route::has($value['rule_as'])){
					$href = URL::route($value['rule_as'],['menu_id'=>$value['rule_id']]);
				}else{
					$href = '#';
				}

				$menu_list .= '<a href="'.$href.'" class="J_menuItem">'.$value['title'].'</a>';
			}

			$menu_list .= '</li>';

		}
		
		if($level > 1)$menu_list .= '</ul>';
		return $menu_list;
	}

	/**
	 * 技术有限，需获取格式为
	 *<li>
	 *	<a href="#">text</a>
	 *  <ul class="nav">
	 *    <li><a href='#'>text</a></li>
	 * 	</ul>
	 *</li>
	 *
	 * 最后生成格式为
	 *<li>
	 *	<a href="#">text</a>
	 *  <ul class="nav">
	 *    <li><a href='#'>text</a></li>
	 * 	</ul>
	 *</li>
	 *</ul>
	 *
	 * 无法判断递归是否为最后一项，导致无法去除最后的ul结束标签
	 * 希望懂得的人赐教
	 * @param  array   $list   需要菜单的列表
	 * @param  integer $level_at 生成菜单层级
	 * @return str           生成的菜单
	 */
	/*public function _getMainMenu($list,$level_at){
		$list = $this->_getMainMenu($list,$level_at);

		var_dump($list);

		if(is_empty($list)){
			return $list;
		}

		$list = substr($list, 0,-5);

		return $list;
	}*/
}