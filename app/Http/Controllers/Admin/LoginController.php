<?php

namespace App\Http\Controllers\Admin;

use Hash;
use URL;
use Admin;
use Illuminate\Http\Request;

class LoginController extends Controller{

	/* 登陆到后台 */
	public function login(Request $request){

		if(Admin::isLogin()) return redirect()->route('admin.home');

		if($request->isMethod('get')){
			return view('admin.login');
		}

		/* 数据验证 */
		if(!$request->has('username')){
			return $this->error('用户名不能为空！');
		}elseif (!$request->has('password')) { 
			return $this->error('密码不能为空！');
		}

		$username = trim($request->input('username'));
		$password = trim($request->input('password'));

		if(strlen($username) > 16){
			return $this->error('用户名不能大于16个字符！');
		}elseif (strlen($username) < 4) {
			return $this->error('用户名不能少于4个字符！');
		}elseif (strlen($password) < 8) {
			return $this->error('密码不能少于8个字符！');
		}

		/* 数据库处理 */
		$user = new \App\Model\AdminUser();
		$info = $user->getUserByPsd($username , $password);

		if($info === false) return $this->error($user->getErr());

		/* 用户登录成功 */

		//把信息标记到session
		$request->session()->put('admin_user_info' , $info);
		
		return $this->success('登录成功！',  URL::route('admin.home'));

	}

	/* 从后台登出 */
	public function logout(Request $request){
		$request->session()->forget('admin_user_info');
		return redirect()->route('admin.login');
	}

	public function mkpsd(){
		var_dump(Hash::check('12345678','$2y$10$z61PFuxiUSXY8NRBb65fMuH2HECEpChv4r8aggeSjQ2/CLyaRbgSG'));
		return Hash::make('12345678');
	}
}