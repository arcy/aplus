<?php
namespace App\Http\Controllers\Admin;

use URL;
use Illuminate\Http\Request;
use App\Model\AdminUser;
use App\Model\AdminRole;
use App\Model\AdminUserRole;
use Admin;
use Hash;

class ManagerController extends Controller{

	public function lists(Request $request){
		/* 实例化模型 */
		$adminUser  = new \App\Model\AdminUser();
		
		$list = $adminUser->page(15);

		$data = ['lists'=>$list];
		return view('admin.manager.manager_list',$data);
	}

	public function add(Request $request){

		if($request->isMethod('get')){
			$data = [];
			return view('admin.manager.manager_add' , $data);
		}

		//处理post数据
		$username = $request->input('username');
		$name = $request->input('name');
		$password = $request->input('password');
		$headimgurl = $request->input('headimgurl');
		$status = ($request->input('status') == 'on') ? 1 : 0;

		if(!between($username, 4,16)){
			return $this->error('用户名介乎4-16个字符！');
		}elseif (!between($name, 2,16)) {
			return $this->error('姓名介乎2-16个字符！');
		}elseif (is_empty($password)) {
			return $this->error('密码不能为空！');
		}elseif (!between($password, 8,30)) {
			return $this->error('密码介乎8-30个字符');
		}elseif (is_empty($headimgurl)) {
			return $this->error('头像不能为空！');
		}elseif ($this->isExistUsername($username)) {
			return $this->error('用户名已存在！');
		}

		$insert = [
			'username'=>$username,
			'name'=>$name,
			'password'=>$password,
			'headimgurl'=>$headimgurl,
			'status'=>$status
		];

		$adminUser = new \App\Model\AdminUser();

		if($adminUser->create($insert)){
			return $this->success('添加成功！',1,URL::route('admin.manager.add'));
		}else{
			return $this->error('添加失败！');
		}
	}

	public function edit(Request $request){
		$id = $request->input('id');

		if(!is_id($id)){
			return $this->error('管理员ID非法！');
		}

		if(Admin::currentAdminId() == $id){
			return $this->error('修改当前用户请在个人资料中修改！');
		}

		// $adminUser = new \App\Model\AdminUser();
		// $info = $adminUser->select(['admin_id','username','name','status','headimgurl'])->find($id);
		$info = \App\Model\AdminUser::select(['admin_id','username','name','status','headimgurl'])->find($id);

		if(is_null($info)){
			return $this->error('管理员不存在！');
		}

		if($request->isMethod('get')){
			$data = ['info'=>$info];
			return view('admin.manager.manager_edit',$data);
		}

		// $username = $request->input('username');
		$name = $request->input('name');
		$password = $request->input('password');
		$headimgurl = $request->input('headimgurl');
		$status = ($request->input('status') == 'on') ? 1 : 0;

		// if(!between($username,4,16)){
		// 	return $this->alertJson('用户名介乎4-16个字符！');
		// }else
		if (!between($name,2,16)) {
			return $this->error('用户姓名介乎2-16个字符！');
		}

		if(!is_empty($password)) {
			if(!between($password,8,16)) return $this->error('用户密码介乎8-16个字符！');
			$adminUser->password = $password;
		}
			
		if(is_empty($headimgurl)){
			return $this->error('用户头像不能为空！');
		}

		$info->name = $name;
		$info->headimgurl = $headimgurl;
		$info->status = $status;
		$info->password = $password;

		if($info->save()){
			return $this->success('修改成功！' , URL::route('admin.manager.list'));
		}else{
			return $this->error('修改失败！');
		}

	}

	public function changeStatus(Request $request){
		$id = $request->input('id');
		$option = $request->input('option');

		if (!is_id($id)) {
			return $this->error('管理员ID非法！');
		}elseif (is_empty($option)) {
			return $this->error('option不能为空！');
		}

		if(Admin::currentAdminId()==$id){
			return $this->error('不能禁用当前登录用户！');
		}

		$adminRule = new \App\Model\AdminUser();
		$status = $adminRule->changeStatus($option,$id);

		if($status === false){
			return $this->error('更新失败！');
		}else{
			return $this->success('更新成功！' , null, ['status'=>$status]);
		}
	}

	public function del(Request $request){
		$id = $request->input('id');
		if (!is_id($id)) {
			return $this->error('管理员ID非法！');
		}

		//管理员删除管理暂时不开放
	}

	/**
	 * 个人信息修改
	 */
	public function editProfile(Request $request){

		//获取信息
		$profile = Admin::currentAdminInfo();

		if($request->isMethod('get')){
			$data = ['info'=>$profile];
			return view('admin.manager.manager_edit_profile',$data);
		}

		$name = $request->input('name');
		$headimgurl = $request->input('headimgurl');

		if (!between($name,2,16)) {
			return $this->error('用户姓名介乎2-16个字符！');
		}
			
		if(is_empty($headimgurl)){
			return $this->error('用户头像不能为空！');
		}

		$info = AdminUser::find(Admin::currentAdminId());
		$info->name = $name;
		$info->headimgurl = $headimgurl;

		if($info->save()){
			Admin::updateLogin($info);
			return $this->success('修改成功！' , URL::route('admin.manager.list'));
		}else{
			return $this->error('修改失败！');
		}
	}

	/**
	 * 修改密码
	 */
	public function editPassword(Request $request){
		if($request->isMethod('get')){
			return view('admin.manager.manager_edit_password');
		}

		$info = AdminUser::find(Admin::currentAdminId());

		$old_pass = $request->input('old_pass');
		$new_pass = $request->input('new_pass');
		$com_pass = $request->input('com_pass');

		if(!between($old_pass,8,16)){
			return $this->error('旧密码介乎8-16个字符！');
		}elseif (!Hash::check($old_pass,$info->password)) {
			return $this->error('旧密码不正确！');
		}elseif (!between($new_pass,8,16)) {
			return $this->error('新密码介乎8-16个字符！');
		}elseif ($new_pass != $com_pass) {
			return $this->error('确认密码和新密码不一致！');
		}

		$info->password = $new_pass;

		if($info->save()){
			Admin::updateLogin($info);
			return $this->success('密码更新成功！', URL::route('admin.manager.editPassword'));
		}else{
			return $this->error('密码更新失败！');
		}
	}

	/**
     * 分配角色
	 */
	public function role(Request $request){
		$id = $request->input('id');
		if(!is_id($id)){
			return $this->error('管理员ID非法！');
		}

		//判断管理员是否存在
		$info = AdminUser::select(['username','admin_id'])->find($id);
		if(!$info){
			return $this->error('管理员不存在！');
		}

		//获取管理员拥有的角色
		$role_ids = [];
		$role_user_list = AdminUserRole::where('admin_id',$id)->first();
		if($role_user_list){
			$role_ids = str_to_arr($role_user_list->role_id);
		}

		//获取所有角色列表
		$role_list = AdminRole::where('status',1)->get();

		//如果不是提交数据，则直接显示模板
		if($request->isMethod('get')){
			$data = ['info'=>$info,'role_ids'=>$role_ids,'role_list'=>$role_list];
			return view('admin.manager.manager_role',$data);
		}

		//提交数据处理
		$role_ids = $request->input('role_id');

		if(is_array($role_ids)){
			$role_ids = implode(',', $role_ids);
		}

		//如果不分配角色则删除
		if(is_empty($role_ids)){
			if(AdminUserRole::where('admin_id',$id)->delete()){
				return $this->success('成功取消分配！',route('admin.manager.list'));
			}else{
				return $this->error('取消分配失败！');
			}
		}

		$update = ['role_id'=>$role_ids,'admin_id'=>$id];
		if(AdminUserRole::updateOrCreate(['admin_id'=>$id],$update)){
			return $this->success('分配成功！',route('admin.manager.list'));
		}else{
			return $this->error('分配失败！');
		}
		
	}

	protected function isExistUsername($username){
		$user = new \App\Model\AdminUser();

		$count = $user->where('username',$username)->count();

		if($count > 0){
			return true;
		}else{
			return false;
		}
	}
}