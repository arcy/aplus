<?php

namespace App\Http\Middleware;

use Closure;
use Admin;
use Route;
use App\Http\Controllers\Controller;

class AdminAuth extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        //检查是否登录
        if(!Admin::isLogin()) return redirect()->route('admin.login');

        //检查是否有当前路由权限
        if(!Admin::checkAuth()) return $this->error('没有权限！');
        return $next($request);
    }

}
