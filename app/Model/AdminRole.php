<?php

namespace App\Model;
use DB;

class AdminRole extends Model{

	protected $table = 'admin_role';

    public $primaryKey = 'role_id';

    protected $guarded = [];

    public $timestamps = false;

    /**
     * 获取角色下的用户列表
     */
    public function getRoleUser($role_id){

    	$users = DB::table('admin_user as u')
            ->join('admin_user_role as ur','u.admin_id','ur.admin_id')
            ->where([
                ['ur.role_id','=',$role_id],
                ['u.status','=',1]
            ])
            ->select(['u.name','u.username','u.headimgurl','u.admin_id'])
            ->get()->toArray();


        if($users){
            $users = array_map(function($el){
                return (array) $el;
            }, $users);

            return $users;
        }
        return [];

    }
}