<?php

namespace App\Model;

class AdminRule extends Model{

	protected $table = 'admin_rule';

    public $primaryKey = 'rule_id';
    
    protected $guarded = [];

    public $timestamps = false;


    public function getUpdatedAtColumn() {
        return null;
    }

}