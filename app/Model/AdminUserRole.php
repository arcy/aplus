<?php

namespace App\Model;

class AdminUserRole extends Model{

	protected $table = 'admin_user_role';

    protected $primaryKey = 'admin_id';

    protected $guarded = [];

    public $timestamps = false;

}