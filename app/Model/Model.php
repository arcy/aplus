<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel{

	/* 错误信息属性 */
	protected $error = null;

	/* 获取错误信息 */
	public function getErr(){
		return $this->error;
	}

	/**
	 * 改变状态值
	 */
	public function changeStatus($field,$id){

		$status = $this->where($this->primaryKey,$id)->value($field);

		if($status == 1){
			$status_val = 0;
		}elseif ($status == 0) {
			$status_val = 1;
		}else{
			return false;
		}

		if($this->where($this->primaryKey,$id)->update([$field=>$status_val])){
			return $status_val;
		}else{
			return false;
		}

	}

	/**
	 * 获取分页类
	 */
	public function page($num=25,$where=null){
		if(is_array($where)){
			$list = $this->where($where)->paginate($num);
		}else{
			$list = $this->paginate($num);
		}

		return $list;
	}

	
}