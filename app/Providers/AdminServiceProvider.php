<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->singleton('AdminService', function () {
            return new \App\Services\AdminService();
        });
    }
}