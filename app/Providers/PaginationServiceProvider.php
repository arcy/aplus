<?php

namespace App\Providers;

use Illuminate\Pagination\PaginationServiceProvider as Pagination;

class PaginationServiceProvider extends Pagination
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // 重写分页类模板地址
        $this->loadViewsFrom(resource_path('views/vendor/pagination'), 'pagination');
    }

}
