<?php

namespace App\Services;

use App\Model\Model;
use DB;
use Route;
use URL;
use App\Model\AdminUser;
use App\Model\AdminRole;
use App\Model\AdminRule;

/**
 * 管理员权限管理服务
 */
class AdminService extends Model{

	 /**
     * 检测用户是否登录
     * @return void
     */
    public function isLogin(){
        if(!session()->has('admin_user_info')){
           return false;
        }

        return true;
    }

    /**
     * 更新缓存管理员资料
     */
    public function updateLogin($data){
        session()->put('admin_user_info',$data);
    }

    /**
     * 获取已登陆用户信息
     * @return array
     */
    public function currentAdminInfo(){
        return session('admin_user_info');
    }

    /**
     * 获取当前登录管理员ID
     * @return [type] [description]
     */
    public function currentAdminId(){
        return $this->currentAdminInfo()->admin_id;
    }

    /**
     * 判断是否超级管理员
     * @return boolean [description]
     */
    public function isSuper(){
        $super_admin_ids = config('admin.super');

        if(in_array($this->currentAdminId(), $super_admin_ids)) return true; 

        return false;
    }

    /**
     * 通过菜单别名检测是否有权限
     */
    public function checkAuth($rule_as = null){

        //判断当前用户是否为超级管理员，如果是超级管理员，则拥有全部权限
        
        if($this->isSuper()){
            return true;
        }

        //如果规则别名为空，则获取当前路由别名
        if(is_empty($rule_as)){
            $rule_as = Route::currentRouteName();
        }

        //判断是否为忽略权限验证的路由
        if(in_array($rule_as, config('admin.ignore_route'))){
            return true;
        }

        $user_model = new AdminUser();
        $rule_model = new AdminRule();


        //根据别名获取规则ID
        $rule_id = $rule_model->where('rule_as',$rule_as)->value('rule_id');

        //别名不存在则判断无权限
        if(!is_id($rule_id)) return false;

        //获取用户所有权限ID
        $rules_ids = $user_model->getUserRule();

        if(in_array($rule_id, $rules_ids)) return true;

        return false;
    }

    /**
     * 获取用户在特定权限下的菜单信息
     */
    public function userMenu($admin_id = null, $parent_id = null){
        if(is_empty($admin_id)) $admin_id = $this->currentAdminId();

        $rule_model = new AdminRule();
        $user_model = new AdminUser();

        $where = [
            ['is_menu', '=', 1],
            ['status', '=', 1]
        ];

        if(is_id($parent_id)){
            $where[] = ['parent_id' , '=' , $parent_id];
        }

        if(!$this->isSuper()){ //如果不是超级管理员则筛选拥有权限的菜单
            //获取当前用户的菜单ID
            $rule_ids = $user_model->getUserRuleId($admin_id);
            if(is_empty($rule_ids)) return [];

            $menus = $rule_model->whereIn('rule_id',$rule_ids)->where($where)->get()->toArray();
        }else{
            $menus = $rule_model->where($where)->get()->toArray();
        }

        return $menus;
    }

    /**
     * 获取内容页的菜单
     */
    public function pageUserMenu($menu_id=null,$admin_id=null){
        // $rule_as = Route::currentRouteName();
        
        if(!is_id($menu_id)){
            $menu_id = request()->input('menu_id');
            !is_id($menu_id) && $menu_id = request()->session()->get('admin_menu_id');
            if(!is_id($menu_id)) return [];
        }

        $rule_model = new AdminRule();

        $info = $rule_model->where('rule_id',$menu_id)->first()->toArray();

        $menus = $this->userMenu($admin_id, $info['rule_id']);

        if ($menus) {
            array_unshift($menus, $info);
        } else {
            $menus[] = $info;
        }

        foreach ($menus as $k => $v) {
            if(Route::has($menus[$k]['rule_as'])){
                $menus[$k]['url'] = URL::route($menus[$k]['rule_as'], ['menu_id'=>$menu_id]);
            }else{
                $menus[$k]['url'] = '#';
            }
        }
        return $menus;
    }
    
}