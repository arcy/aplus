<?php

/**
 *函数功能:判断字符串是否为正整数
 *参数表: string $val
 *返回值: bool
 */
function is_id($val){
   if(!is_numeric($val) || strpos($val,'.') !== false){
	   return false;
   }else{
	   $val = (int)$val;
	   if($val > 0){
		   return true;
	   }else{
		   return false;
	   }
   }
}

/**
 * 进行字符串转义
 */
function str_escape($str){
	$str = htmlspecialchars($str);
	$str = nl2br(str_replace(' ','&nbsp;',$str));
	return $str;
}

/**
 * 将字符串分隔成数组
 * str_split函数的扩展
 * @param string $string
 * @return array
 */
function mb_str_split( $string ) {
    # Split at all position not after the start: ^
    # and not before the end: $
    return preg_split('/(?<!^)(?!$)/u', $string );
}

 /**
  * 根据前后字符串截取中间段字符
  * 2015-1-28进行在重复出现字符时不能正常截取的bug修复
  * 2015-3-16 修复参数$star_str在字符串首位stripos返回0误处理为找不到字符串的bug
  */
function cut_str($str,$star_str,$end_str,$ignore=false){
	$start = 0;
	$s_pos = stripos($str,$star_str);
	
	// 2015-3-16 lht
	// BEGIN
	// if(!$s_pos) return null;
	if (FALSE === $s_pos) 
	{
		return null;
	}
	//
	// END

	$n_str = substr($str ,$s_pos+strlen($star_str) );
	$e_pos = stripos($n_str ,$end_str);

	if(!$e_pos) return null;
	$e_str = substr($n_str,$start,$e_pos);
	if(!$ignore){
		// $start = strlen($star_str);
		// $e_pos = $e_pos - $start;
		$e_str = $star_str . $e_str;
	}
	return $e_str;
}

/**
 * 检测是否为空或未定义
 */
function is_empty($val){
	if($val == 0 && $val!='') return false;
	if(!isset($val) || is_null($val) || empty($val)){
		return true;
	}else{
		return false;
	}
}

/**
 * 检测一个字符串是否是邮件地址格式
 * @param string $value 待检测字符串
 * @return boolean
 */
function is_email($value) {
    return preg_match("/^[0-9a-zA-Z]+(?:[\_\.\-][a-z0-9\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+$/i", $value);
}

/*
 * 验证手机号的正确性
 * @param $tel
 * @return boolean
 */    
function is_tel($tel){
    if (is_empty($tel)){
        return false;
    }
    $match = preg_match("/^(\+?\d{2,3})?0?1(3\d|47|5\d|8\d)\d{8}$/", $tel);
	$match2 = preg_match("/^0\d{2,3}-?\d{7,8}$/",$tel);
    if($match === 1 || $match2 === 1){
        return true;
    }
    return false;
}

/*
 * 验证日期格式(Y-m-d[ H:i:s])是否正确
 * @param $data
 * @return boolean
 */
function is_date($date) {    
    // match the format of the date
	$isDate = true;
	$isTime = true;
	$date = explode(' ',$date);
	//检验日期
    if (preg_match("/^([0-9]{4})-{0,1}([0-9]{2})-{0,1}([0-9]{2})$/", $date[0], $parts)) {  
        // check whether the date is valid of not  
        if (!checkdate($parts[2], $parts[3], $parts[1])) {
            $isDate = false;     
	    }
    }else{  
    	$isDate = false;    
    }
	//检验时间
	if(isset($date[1]) && !empty($date[1])){
	    if (preg_match("/^(\d{2})\:(\d{2})\:(\d{2})$/", $date[1], $timeArr)) {
			if(is_numeric($timeArr[0]) && is_numeric($timeArr[1]) && is_numeric($timeArr[2])){
				if (($timeArr[0] >= 0 && $timeArr[0] <= 23) && ($timeArr[1] >= 0 && $timeArr[1] <= 59) && ($timeArr[2] >= 0 && $timeArr[2] <= 59)){
					$isTime = true;
				}else{
					$isTime = false;
				}
			}
		}else{
			$isTime = false;
		}
	}
	
	return ($isDate && $isTime);
}

/*
 * 获取日期星期几(Y-m-d)
 * @param $date
 * @return 
 */
function date_to_weekday($date) {

       $datearr = explode("-", $date);     //将传来的时间使用“-”分割成数组

       $year = $datearr[0];       //获取年份

       $month = sprintf('%02d', $datearr[1]);  //获取月份

       $day = sprintf('%02d', $datearr[2]);      //获取日期

       $hour = $minute = $second = 0;   //默认时分秒均为0

       $dayofweek = mktime($hour, $minute, $second, $month, $day, $year);    //将时间转换成时间戳

       $xingqiji=date("w", $dayofweek);   //获取星期值

       $weekarray=array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
       return $weekarray[$xingqiji];

}

/**
 * 多层加密
 */
function get_hash($val){
	return sha1(md5(base64_encode($val)));
}

/**
 * 无限极菜单分类
 * @param array $data 需要递归的数组，取地址
 * @param int $pid 上级菜单
 * @param int $count 当前级数
 * @return array $treelist
 */
function get_tree($data,$id='id',$pkey='pid',$pid = 0,$count = 1){
    static $treeList = array();
    foreach ($data as $key => $value){
        if($value[$pkey]==$pid){
            $value['Count'] = $count;
            $treeList[]=$value;
            unset($data[$key]);
            get_tree($data,$id,$pkey,$value[$id],$count+1);
        } 
   }
   return $treeList ;
}


/**
 * 数组去重
 * @param array $arr需要去重的数组
 * @param string $key不能重复的字段
 * @return array
 */
function assoc_unique($arr, $key){
  $tmp_arr = array();
  foreach($arr as $k => $v){
    if(in_array($v[$key], $tmp_arr)){    //搜索$v[$key]是否在$tmp_arr数组中存在，若存在返回true               
        unset($arr[$k]);
    }else{
        $tmp_arr[] = $v[$key];
    }
  }
  sort($arr); //sort函数对数组进行排序
  return $arr;
}

/**
 * AES加密
 * @param string $data 需要加密/解密的字符串
 * @param boolean $type true为加密，false为解密
 * @param string $key 加密/解密所需的密匙
 * @param string $iv 反向密匙
 * @return string $string 加密/解密后的字符串
 */
function AES($data,$type=true,$key=null,$iv=null){
	
	is_empty($key) ? $privateKey = "3825libtop026000" : $privateKey = $key;
	is_empty($iv)  &&  $iv  = "3825libtop026000";

	if($type){ //加密

		$encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $privateKey, $data, MCRYPT_MODE_CBC, $iv);
		return base64_encode($encrypted);
	}else{ //解密

		$encryptedData = base64_decode($data);
		$decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $privateKey, $encryptedData, MCRYPT_MODE_CBC, $iv);
		$decrypted = str_replace(array("<br>" , "\0") , "" , $decrypted);
		
		return $decrypted;
	}
}

/**
 * AES加密适合URL使用的字符串
 * 参数同上
 */
function AES_URL($data,$type=true,$key=null,$iv=null){
	$find = array('-1x','-2x' , '-3x');
	$replace = array('/' , '=' , '+');
	if($type == false){ //如果是解密的
		$data = str_replace($find, $replace, $data);
	}

	$str = AES($data , $type , $key , $iv);

	if($type === true){
		$str = str_replace($replace, $find, $str);
	}

	return $str;
}

/**
 * 查看字符串是否在两个数字之间
 */
function between($str,$min,$max){
	if(mb_strlen($str,'UTF-8') >= $min && mb_strlen($str,'UTF-8') <= $max ){
		return true;
	}

	return false;
}

/**
 * 字符串长度少于判断
 */
function str_lt($str,$len){
	if(mb_strlen($str,'UTF-8') <= $len){
		return true;
	}

	return false;
}

/**
 * 字符串长度大于判断
 */
function str_gt($str,$Len){
	if(mb_strlen($str,'UTF-8') >= $len){
		return true;
	}

	return false;
}

/**
 * 错误返回,已被废除
 */
// function error_back($message){
// 	return redirect()->back()->withInput()->with('bk_error_message',$message);
// }

/**
 * 字符串变为数组
 */
function str_to_arr($str){
	if(!is_empty($str)){
		if(strpos(',', $str)){
			$str[] = $str;
		}else{
			$str = explode(',', $str);
		}
	}else{
		$str = [];
	}

	return $str;
}