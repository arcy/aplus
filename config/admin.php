<?php

return [
	//超级管理员帐号
	'super' => [1],
	//忽略权限验证的路由
	'ignore_route' => ['home','home.index']
];