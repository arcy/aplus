<?php

return [

	/* 操作系统 */
	'php_os' => PHP_OS,
	/* php运行方式 */
	'php_sapi' => php_sapi_name(),
	/* 运行环境 */
	'run_env' => env("SERVER_SOFTWARE","无法检测"),
	/* 产品名称 */
	'product_name' => '中山世宇动漫科技有限公司-售后服务中心系统',
	/* 产品流水号 */
	'product_num' => '20170310-dev',
	/* 上次附件限制 */
	'upload_max_filesize' => ini_get('upload_max_filesize'),
	/* 执行时间限制 */
	'max_execution_time' => ini_get('max_execution_time') . "秒",
	/*剩余空间*/
	'disk_free_space' => round((@disk_free_space(".") / (1024 * 1024)), 2),
	/* 全部空间 */
	'disk_total_space' => round((@disk_total_space(".") / (1024 * 1024)), 2),

	/* 团队名称 */
	'team_name' => 'IT部',
	'product_author' => [
		['name'=>'谭汉亮', 'url'=>'http://www.arcy.cn', 'email'=>'arcy@ctstudio.cn']
	],

];