-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2017-04-18 15:32:09
-- 服务器版本： 5.6.23-log
-- PHP Version: 5.6.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `console_auth_admin`
--

-- --------------------------------------------------------

--
-- 表的结构 `ac_admin_role`
--

CREATE TABLE IF NOT EXISTS `ac_admin_role` (
  `role_id` mediumint(4) NOT NULL,
  `name` varchar(60) NOT NULL COMMENT '角色名',
  `remark` varchar(288) DEFAULT NULL,
  `rule_id` varchar(250) NOT NULL COMMENT '规则id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

--
-- 转存表中的数据 `ac_admin_role`
--

INSERT INTO `ac_admin_role` (`role_id`, `name`, `remark`, `rule_id`, `status`) VALUES
(1, '文章发布员', '测试一下', '1,2,3', 1);

-- --------------------------------------------------------

--
-- 表的结构 `ac_admin_rule`
--

CREATE TABLE IF NOT EXISTS `ac_admin_rule` (
  `rule_id` mediumint(4) NOT NULL,
  `title` varchar(16) NOT NULL COMMENT '菜单名',
  `rule_as` char(88) DEFAULT NULL COMMENT '别名',
  `icon` varchar(88) DEFAULT NULL COMMENT '图标',
  `parent_id` mediumint(4) NOT NULL DEFAULT '0' COMMENT '上级ID',
  `is_auth` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否权限',
  `is_menu` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否菜单',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='权限规则表';

--
-- 转存表中的数据 `ac_admin_rule`
--

INSERT INTO `ac_admin_rule` (`rule_id`, `title`, `rule_as`, `icon`, `parent_id`, `is_auth`, `is_menu`, `status`) VALUES
(1, '系统设置', NULL, 'fa fa-dashboard', 0, 0, 1, 1),
(2, '菜单管理', 'admin.auth.rule.list', NULL, 1, 1, 1, 1),
(3, '添加菜单', 'admin.auth.rule.add', NULL, 2, 1, 1, 1),
(4, '改变菜单状态', 'admin.auth.rule.changeStatus', NULL, 2, 1, 0, 1),
(5, '删除菜单', 'admin.auth.rule.del', NULL, 2, 1, 0, 1),
(6, '权限管理', NULL, 'fa fa-key', 0, 0, 1, 1),
(7, '管理员管理', 'admin.manager.list', NULL, 6, 1, 1, 1),
(8, '角色管理', 'admin.auth.role.list', NULL, 6, 1, 1, 1),
(9, '添加管理员', 'admin.manager.add', NULL, 7, 1, 1, 1),
(10, '添加角色', 'admin.auth.role.add', NULL, 8, 1, 1, 1),
(11, '改变管理员状态', 'admin.manager.changeStatus', NULL, 7, 1, 0, 1),
(12, '修改管理员信息', 'admin.manager.edit', NULL, 7, 1, 0, 1),
(13, '编辑角色', 'admin.auth.role.edit', NULL, 8, 1, 0, 1),
(14, '删除角色', 'admin.auth.role.del', NULL, 8, 1, 0, 1),
(15, '修改角色状态', 'admin.auth.role.changeStatus', NULL, 8, 1, 0, 1),
(16, '修改个人信息', 'admin.manager.editProfile', NULL, 7, 1, 0, 1),
(17, '修改个人密码', 'admin.manager.editPassword', NULL, 7, 1, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ac_admin_user`
--

CREATE TABLE IF NOT EXISTS `ac_admin_user` (
  `admin_id` mediumint(4) NOT NULL,
  `username` char(16) NOT NULL COMMENT '用户名',
  `name` varchar(16) NOT NULL COMMENT '用户姓名',
  `password` char(64) CHARACTER SET utf8 NOT NULL COMMENT '密码',
  `headimgurl` varchar(255) NOT NULL COMMENT '用户头像',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户时间'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='后台用户表';

--
-- 转存表中的数据 `ac_admin_user`
--

INSERT INTO `ac_admin_user` (`admin_id`, `username`, `name`, `password`, `headimgurl`, `status`, `create_at`) VALUES
(1, 'arcy', 'arcytan', '$2y$10$l78s/ohWEX7AUk/fFh.eteUy.X968kbM1HMcQKyff9TiojHqRXReS', '/admin/upload/headimg/2017-03-20-58cf48b413f7f.jpg', 1, '2017-03-06 09:29:36'),
(2, 'arcytan', 'liangshao', '$2y$10$npabTDwBNPBh2rkuSeIbjOTXAGW3Me31W8o2fAHQUVSOaz1MmVuH6', '/admin/upload/headimg/2017-03-20-58cfa006be9a1.png', 1, '2017-03-16 09:29:28'),
(7, 'arcy1', 'arcytan1', '$2y$10$vVc3wX6.mgtTkYVHutLuJeBAthFUWQKIihHzEngFESjKU.K2nCo/u', '/admin/upload/headimg/2017-03-20-58cf76efbbf40.jpg', 1, '2017-03-20 06:53:06');

-- --------------------------------------------------------

--
-- 表的结构 `ac_admin_user_role`
--

CREATE TABLE IF NOT EXISTS `ac_admin_user_role` (
  `admin_id` mediumint(4) NOT NULL,
  `role_id` mediumint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色分配表';

--
-- 转存表中的数据 `ac_admin_user_role`
--

INSERT INTO `ac_admin_user_role` (`admin_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ac_admin_role`
--
ALTER TABLE `ac_admin_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `ac_admin_rule`
--
ALTER TABLE `ac_admin_rule`
  ADD PRIMARY KEY (`rule_id`);

--
-- Indexes for table `ac_admin_user`
--
ALTER TABLE `ac_admin_user`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `password` (`password`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ac_admin_role`
--
ALTER TABLE `ac_admin_role`
  MODIFY `role_id` mediumint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ac_admin_rule`
--
ALTER TABLE `ac_admin_rule`
  MODIFY `rule_id` mediumint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `ac_admin_user`
--
ALTER TABLE `ac_admin_user`
  MODIFY `admin_id` mediumint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
