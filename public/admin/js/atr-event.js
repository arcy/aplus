(function($) {
"use strict";
$(document).ready(function(){

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

//表单提交事件
if($('form[data-event=onform]').length > 0){
	$('form[data-event=onform]').submit(function(){
		return false;
	})
	jQuery.ajax({
	    url: CT.JS+'Validform_v5.3.2.js',
	    dataType: "script",
	     //cache: true
	}).done(function() {
	    $('form[data-event=onform]').Validform({
			tiptype:function(msg,o,cssct){
				if(o.type == 1 || o.type == 3){
					o.obj.parents('.form-group').addClass('has-error');
					tips(msg,function(){
						o.obj.focus();
					});
				}else{
					o.obj.parents('.form-group').removeClass('has-error').addClass('has-success');
				}
			},
			beforeSubmit:function(curform){
				var callback = $(curform).attr('data-callback');
				if(callback){
					callback = (new Function("return " + callback))();
					callback();
				}
				ajaxSubmit(curform);
				return false;
			}
		});
	});
}


$('[data-event=onrefresh]').click(function(){
	window.location.reload();
})

//弹出框
$('.ondialog').click(function(event){
	event.preventDefault();
	var content = $(this).attr('dialog-content'),
	    url = $(this).attr('dialog-url'),
	    id = $(this).attr('dialog-id'),
	    title = $(this).attr('dialog-title');
	var config = {
		"title" : title,
		"content" : content,
		"fixed" : true,
		cancelValue : '关闭',
		cancel : true,
		autofocus : true
		// "quickClose" : true,
	}

	if(url) config.url = url;
	if(id) config.id = id;

	var d = dialog(config);
	d.showModal();
})

//改变状态值
$('[data-event=onstatus]').click(function(event){
	event.preventDefault();
	var url   = $(this).attr('href');
	var $this = $(this);
	var data = [];
	$.ajax({
		url:url,
		type:'POST',
		dataType:"json",
		data:data,
		error: function(status,msg){
		    tips(msg);
	    },
		success:function(reVal){
			
			if(reVal.code == 1){
				if(reVal.data.status == 1){
					$this.find('i').attr('class','fa fa-check');
				}else{
					$this.find('i').attr('class','fa fa-close');
				}
				
			}else{
				tips(reVal.msg);
			}
		}
	})
})

//删除信息
$('[data-event=ondelete]').click(function(event){
	event.preventDefault();
	var url = $(this).attr('href');
	var text = $(this).attr('data-text');
	var data = [];
	parent.layui.use('layer', function(){
		var layer = parent.layui.layer;
	  
		layer.confirm('确认删除“'+text+'”？', {'icon': 3, 'title':'确认提示'}, function(index){
			$.ajax({
				url:url,
				type:'POST',
				dataType:"json",
				data:data,
				error: function(status,msg){
				    tips(msg);
			    },
				success:function(reVal){
					tips(reVal.msg);
					if(reVal.code == 1){
						if(reVal.url){
							window.location.href = reVal.url;
						}else{
							window.location.reload();
						}
					}
				}
			})
			layer.close(index);
		});
	});
})

//打开dialog
$('[data-event=onsubmit]').click(function(event){
	event.preventDefault();
	var url = $(this).attr('href');
	var title = $(this).attr('data-title');
	var data = [];
	layui.use('layer', function(){
		var layer = layui.layer;
	  	
	  	layer.open({
			id: 'onsubmitdialog',
			title: title,
			type: 2, 
  			content: url,
  			shadeClose: true,
  			resize: false,
  			success: function(layero, index){
  				layer.iframeAuto(index);
  			}
		});
		
	});
})

})

})(jQuery);