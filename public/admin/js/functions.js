/**
 *提交表单数据
 *@data 提交的数据、提交的对象
 *@url 提交地址
 *@bntObj 提交按钮
 */
function ajaxSubmit(data,url,bntObj){
	if(!data||data==''){
		if(!url||url=='') url=$("form").eq(0).prop('action');
		if(!bntObj || bntObj=='') bntObj = $("form").eq(0).find('[type=submit]');
        data = $("form").serialize();
    }

	if(typeof(data) == 'object'){
		if(!url||url=='') url=data.prop('action');
		if(!bntObj || bntObj=='') bntObj = data.find('[type=submit]');
		data = data.serialize();
	}
	
    if(!url||url==''){
        url=document.URL;
    }
	
	var bntVal = bntObj.val();
	bntObj.val('提交中').attr('disabled',true);
	
	$.ajax({
		url:url,
		type:'POST',
		dataType:"json",
		data:data,
		error: function(status,msg){
			bntObj.val(bntVal).attr('disabled',false);
		    tips(msg);
	    },
		success:function(reVal){
			tips(reVal.msg);
			if(reVal.url){
				setTimeout(function(){window.location.href = reVal.url},1000);
			}
			bntObj.val(bntVal).attr('disabled',false);
		}
	})
}

/**
 *
 *
 */
function tips(content,callback){
	if(document.getElementById('toasting')){
		$('#toasting').remove();
	}
	var el = $('<div></div>');
	el.attr({id:'toasting'}).html(content).appendTo('body');
	$('#toasting').css({
		'padding':'10px 15px',
		'border':'2px solid rgba(0,0,0,.2)',
		'background-color':'rgba(0,0,0,.6)',
		'border-radius':'5px',
		'font-size':'13px',
		'color':'#FFF',
		'position':'absolute',
		'z-index':99999,
		'opacity':0,
	})
	var w = $('#toasting').innerWidth(),
	    h = $('#toasting').innerHeight(),
		ow = $(window).width(),
		oh = $(window).height(),
		top = (oh-h)/2 + $(window).scrollTop(),
		left = (ow-w)/2;
	$('#toasting').css({'left':left , 'top':top-30});
	$('#toasting').animate({'top':top,'opacity':1});
	//删除提示框
	setTimeout(function(){
		$('#toasting').fadeOut(null,null,function(){
			$('#toasting').remove();
			if(!!callback) callback();
		})
	},1000)
}

/**
 *图片预览
 */
function image_preview(src){
	window.top.$.dialog({
		title:'图片预览',
		id:'image_preview',
		lock:true,
		fixed:true,
		resize:false,
		content:'<img src="'+src+'"/>'
	})
}

/**
 *预加载图片
 */
function imgLoad(url, callback) {
	var img = new Image();

	img.src = url;
	if (img.complete) {
		callback(img.width, img.height);
	} else {
		img.onload = function () {
			callback(img.width, img.height);
			img.onload = null;
		};
	};

};

/**
 * 工具集
 */
window.tools = {
	/**
	 * 弹出层
	 */
	'dialog' : function(config){
		config.shade = 0.6;
		parent.layui.use('layer',function($arg){
			var layer = parent.layui.layer;
			layer.open(config);
		})
	} 
}


function create_tree(data,el_id,url){
	jQuery.ajax({
	    url: CT.JS+'plugins/treeview/bootstrap-treeview.js',
	    dataType: "script",
	     //cache: true
	}).done(function() {

		var tree = [];
		if(!!data){
			tree = data;
		}else{
			tree = getJsonByUrl(url);
		}
		
	    $(el_id).treeview({
            showCheckbox: true,
            data:tree,
            levels: 5,
            multiSelect: true,
            highlightSelected: false,
            checkedIcon: 'glyphicon glyphicon-check',
            uncheckedIcon: 'glyphicon glyphicon-unchecked',
            onNodeChecked: function(event, node) { //选中节点
                //获取所有父节点
                var parentNodes = getParentIdArr(node,el_id);
                var selectNodes = getNodeIdArr(node);//获取所有子节点

                selectNodes = selectNodes.concat(parentNodes);
                if(selectNodes){ //子节点不为空，则选中所有子节点
                    $(el_id).treeview('checkNode', [ selectNodes,{ silent: true }]);
                }
            },
            onNodeUnchecked: function (event, node) { //取消选中节点
                //获取为选取子节点的父节点
                var parentNodes = getUnParentIdArr(node , el_id);
                var selectNodes = getNodeIdArr(node);//获取所有子节点

                selectNodes = selectNodes.concat(parentNodes);
                if(selectNodes){ //子节点不为空，则取消选中所有子节点
                    $(el_id).treeview('uncheckNode', [ selectNodes,{ silent: true }]);
                }
            }
        });
	});

	//递归获取所有的结点id
    function getNodeIdArr( node ){
            var ts = [];
            if(node.nodes){
                for(x in node.nodes){
                    ts.push(node.nodes[x].nodeId);
                    if(node.nodes[x].nodes){
                    var getNodeDieDai = getNodeIdArr(node.nodes[x]);
                        for(j in getNodeDieDai){
                            ts.push(getNodeDieDai[j]);
                        }
                    }
                }
            }else{
                ts.push(node.nodeId);
           }
       return ts;
    }

    //获取所有多余的父节点
    function getParentIdArr(node,treeview_id){
        var ts = [];
        if(node.parentId !== undefined){
            var parentNode = $(treeview_id).treeview('getNode', node.parentId);
            ts.push(node.parentId);
            var a = getParentIdArr(parentNode,treeview_id);
            ts.push.apply(ts,a);
        }

        return ts;
    }

    //获取多余的unchecked父节点
    function getUnParentIdArr(node,treeview_id){
        var ts=[];
        //非root节点
        if(node.parentId !== undefined){
            //获取同级元素
            var siblingsNodes = $(treeview_id).treeview('getSiblings',node.nodeId);
            ts.push(node.parentId);
            for(x in siblingsNodes){
                if(siblingsNodes[x].state.checked){
                    ts.splice($.inArray(node.parentId, ts), 1); //删除数组元素
                    break;
                }else{
                    var a = getUnParentIdArr($(treeview_id).treeview('getNode',node.parentId),treeview_id);
                    ts.push.apply(ts,a);
                }
            }

            
        }

        return ts;
    }
}

function getJsonByUrl(url,data){
	var json = [];
	if(!data) data = [];
	$.ajax({
		url:url,
		type:'POST',
		dataType:"json",
		data:data,
		error: function(status,msg){
		    tips(msg);
	    },
		success:function(reVal){
			json = reVal;
		}
	});

	return json;
}
