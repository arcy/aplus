@extends('admin.layouts.app')
@section('css')
<link href="/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
@endsection
@section('main')
<form method="post" class="form-horizontal" data-event="onform" data-callback="get_checked_id">
{{ csrf_field() }}
<div class="row">
    <div class="col-sm-6">
        <div class="role-setting">
            <div class="ibox-title">
                <h4>权限设置</h4>
            </div>

            <!-- 权限列表 -->
            <div class="ibox-content">
                <div id="rule-tree"></div>
            </div>
            <!-- //权限列表 -->
        </div>
    </div>

    <div class="col-sm-6">
        <div class="ibox-title">
            <h4>角色信息</h4>
        </div>

        <!-- 角色信息 -->
        <div class="ibox-content">
            <div class="form-group">
                <label class="col-sm-3 control-label">角色名</label>
                <div class="col-sm-9">
                    <input value="{{ $info->name }}" type="text" name="name" datatype="s1-16" nullmsg="角色名不能为空" errormsg="角色名在1-16个字符之间" class="form-control" data-trigger="hover">
                    <span class="help-block m-b-none">介乎1-16个字符</span>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-sm-3 control-label">备注</label>
                <div class="col-sm-9">
                    <textarea name="remark" datatype="s1-288" errormsg="备注在1-288个字符之间" class="form-control" ignore="ignore">{{ $info->remark }}</textarea>
                    <span class="help-block m-b-none">不填写可留空</span>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-sm-3 control-label">是否启用</label>

                <div class="col-sm-9">
                    <input type="checkbox" name="status" class="js-switch-2" @if($info->status == 1) checked @endif/>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <input type="hidden" name="rule_id" value="">
                    <button class="btn btn-primary" type="submit">保存</button>
                </div>
            </div>
        </div>
        <!-- //角色信息 -->
    </div>
</div>
</form>

@endsection
@section('script')
<script type="text/javascript">
	//swich按钮
	$(function($){
		var elem = document.querySelector('.js-switch');
		var init = new Switchery(elem);

		var elem2 = document.querySelector('.js-switch-2');
		var init2 = new Switchery(elem2);

        //创建树形权限选择
        var tree_data = {!! $rule_list !!};
		create_tree(tree_data,'#rule-tree');

	})

    function get_checked_id(){
        var nodeid = [];
        var ruleid = [];
        $('#rule-tree').find('.node-checked').each(function(){
            if($(this).attr('data-nodeid')) nodeid.push($(this).attr('data-nodeid'));
        })

        for(var i=0;i<nodeid.length;i++){
            var node = $('#rule-tree').treeview('getNode', nodeid[i]);
            if(node.rule_id) ruleid.push(node.rule_id);
        }
        $('input[name=rule_id]').val(ruleid.join(','));
    }
   
</script>
<script src="/admin/js/plugins/switchery/switchery.js"></script>
<script src="/admin/js/plugins/treeview/bootstrap-treeview.js"></script>
@endsection