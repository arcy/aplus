@extends('admin.layouts.app')
@section('main')

<div class="table-responsive">
    <table class="table table-bordered table-striped">
    	<thead>
            <tr>
                <th>#</th>
                <th>角色名称</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lists as $list)
            <tr>
                <td>{{ $list->role_id }}</td>
                <td>{{ $list->name }}</td>
                <td>
                    @if($list->status == 1)
                    <a href="{{ URL::route('admin.auth.role.changeStatus',['option'=>'status','id'=>$list->role_id]) }}" data-event="onstatus"><i class="fa fa-check"></i></a>
                    @else
                    <a href="{{ URL::route('admin.auth.role.changeStatus',['option'=>'status','id'=>$list->role_id]) }}" data-event="onstatus"><i class="fa fa-close"></i></a>
                    @endif
                </td>
                <td>
                    <!-- <a href="{{ URL::route('admin.auth.role.edit',['id'=>$list->role_id]) }}" class="btn btn-white btn-bitbucket btn-sm"><i class="fa fa-user"></i> 成员管理</a> -->
                    <a href="{{ URL::route('admin.auth.role.edit',['id'=>$list->role_id]) }}" class="btn btn-white btn-bitbucket btn-sm"><i class="fa fa-paste"></i> 编辑</a>
                    <a href="{{ URL::route('admin.auth.role.del',['id'=>$list->role_id]) }}"  data-event="ondelete" data-text="{{ $list->name }}" class="btn btn-white btn-bitbucket btn-sm"><i class="fa fa-exclamation-circle"></i> 删除</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-sm-12">
        {{$lists->links()}}
    </div>
</div>
@endsection