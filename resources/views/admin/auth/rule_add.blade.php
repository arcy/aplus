@extends('admin.layouts.app')
@section('css')
<link href="/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
@endsection
@section('main')

<form method="get" class="form-horizontal" data-event="onform">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="col-sm-2 control-label">上级菜单</label>

        <div class="col-sm-10">
            <select class="form-control m-b" name="parent_id" datatype="n1-3" errormsg="请选择上级权限（菜单）">
                <option value="0">作为一级菜单</option>
                {!! $menu_str !!}
            </select>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">菜单名</label>
        <div class="col-sm-10">
            <input type="text" name="title" datatype="s1-16" nullmsg="菜单名不能为空" errormsg="权限名在1-16个字符之间" class="form-control" data-trigger="hover">
            <span class="help-block m-b-none">当作为菜单显示时，则显示此名称</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">菜单别名</label>
        <div class="col-sm-10">
            <input type="text" name="rule_as" datatype="s3-60" nullmsg="菜单别名不能为空" errormsg="权限别名介乎3-60个字符" class="form-control" data-trigger="hover" ignore="ignore">
            <span class="help-block m-b-none">别名为路由名称，如admin.auth.rule.list，非权限菜单可留空</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">图标</label>

        <div class="col-sm-6">
   			<input type="text" name="icon" class="form-control" >
            <span class="help-block m-b-none">仅在一级菜单中显示</span>
        </div>
        <div class="col-sm-4">
        	<i class="" id="icon-show" style="margin-right: 18px;"></i>
        	<button data-toggle="dropdown" type="button" class="btn btn-outline btn-primary "><i class="fa fa-cog"></i> 选择图标</button>

        	<!-- 图标列表 -->
            <ul id="icon-wrap" class="icon-wrap dropdown-menu dropdown-menu-left">
                <li class="J_tabShowActive">
                	@foreach($icon as $val)
                	<a href="javascript:"><i class="{{ $val }}"></i></a>
                	@endforeach
                </li>
            </ul>
            <!-- //图标列表结束 -->
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">是否显示</label>

        <div class="col-sm-10">
            <input type="checkbox" name="is_menu" class="js-switch" checked/>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">是否启用</label>

        <div class="col-sm-10">
            <input type="checkbox" name="status" class="js-switch-2" checked/>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">是否权限</label>

        <div class="col-sm-10">
            <input type="checkbox" name="is_auth" class="js-switch-3" checked/>
            <span class="help-block m-b-none">设为权限则会判断是否有权限进入</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <button class="btn btn-primary" type="submit">保存</button>
        </div>
    </div>
</form>
            

@endsection
@section('script')
<script type="text/javascript">
	//swich按钮
	$(function($){
		var elem = document.querySelector('.js-switch');
		var init = new Switchery(elem);

		var elem2 = document.querySelector('.js-switch-2');
		var init2 = new Switchery(elem2);

        var elem2 = document.querySelector('.js-switch-3');
        var init2 = new Switchery(elem2);

		$('#icon-wrap').find('a').click(function(){
			var cs = $(this).find('i').attr('class');
			$('input[name=icon]').val(cs);
			$('#icon-show').attr('class',cs);
		})
	})
	
</script>
<script src="/admin/js/plugins/switchery/switchery.js"></script>
@endsection