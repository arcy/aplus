@extends('admin.layouts.app')
@section('main')
<div class="table-responsive">
    <table class="table table-bordered table-striped">
    	<thead>
            <tr>
                <th>#</th>
                <th>菜单名</th>
                <th>别名</th>
                <th>显示</th>
                <th>图标</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                {!! $rule_str !!}
            </tr>
        </tbody>
    </table>
</div>
@endsection