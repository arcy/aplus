<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>A+ 管理后台</title>

    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/admin/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="/admin/css/animate.min.css" rel="stylesheet">
    <link href="/admin/js/plugins/layui/css/layui.css" rel="stylesheet">
    <link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="/admin/css/aplus.style.css?v=4.1.0" rel="stylesheet">
</head>

<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="logo-normal">
                            <img src="/admin/img/logo2.png" alt="">
                        </div>
                        <div class="logo-element">A+</div>
                    </li>
                    
                    <!-- 生成菜单 -->
                    {!! $menu_str !!}
                    <!-- //生成菜单 -->
                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <!-- 全局搜索框 -->
                        <!-- <form role="search" class="navbar-form-custom" method="post" action="#">
                            <div class="form-group">
                                <input type="text" placeholder="请输入关键字" class="form-control" name="top-search" id="top-search">
                            </div>
                        </form> -->
                        <!-- 全局搜索框结束 -->
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown hidden-xs  profile-element">
                            <span class="clear"></span>
                            <span class="pull-left">
                                <img alt="image" class="img-circle" src="{{ $SELF->headimgurl }}" />
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle pull-right" href="#">
                                <span class="block m-t-xs" style="margin-top:16px;"><strong class="font-bold">{{ $SELF->name }}<b class="caret"></b></strong></span>
                                <!-- <span class="text-muted text-xs block">超级管理员<b class="caret"></b></span> -->
                            </a>
                            <span class="clear"></span>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="J_menuItem" href="{{ URL::route('admin.manager.editPassword') }}">修改密码</a>
                                </li>
                                <li><a class="J_menuItem" href="{{ URL::route('admin.manager.editProfile') }}">修改资料</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="{{ URL::route('admin.logout') }}">安全退出</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row content-tabs">
                <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
                </button>
                <nav class="page-tabs J_menuTabs">
                    <div class="page-tabs-content">
                        <a href="javascript:;" class="active J_menuTab" data-id="{{ URL::route('admin.home.index') }}">首页</a>
                    </div>
                </nav>
                <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
                </button>
                <div class="btn-group btn-group-op roll-nav roll-right">
                    <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span>

                    </button>
                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
                        <li class="J_tabShowActive"><a>定位当前选项卡</a>
                        </li>
                        <li class="divider"></li>
                        <li class="J_tabCloseAll"><a>关闭全部选项卡</a>
                        </li>
                        <li class="J_tabCloseOther"><a>关闭其他选项卡</a>
                        </li>
                    </ul>
                </div>
                <div class="btn-group roll-nav roll-right">
                    <button class="dropdown J_tabClose" data-toggle="dropdown">清除缓存<span class="caret"></span>

                    </button>
                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
                        <li><a>站点数据</a>
                        </li>
                        <li class="divider"></li>
                        <li><a>站点模板</a>
                        </li>
                        <li class="divider"></li>
                        <li><a>网站日志</a>
                        </li>
                    </ul>
                </div>
                <a href="{{ URL::route('admin.logout') }}" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
            </div>
            <div class="row J_mainContent" id="content-main">
                <!-- 正式内容iframe -->
                <iframe class="J_iframe" name="iframe-1" width="100%" height="100%" data-id="{{ URL::route('admin.home.index') }}" src="{{ URL::route('admin.home.index') }}" frameborder="0" seamless></iframe>
                <!-- //正式内容iframe结束 -->
            </div>
            <div class="footer">
                <div class="pull-right">&copy; 2014-2015 <a href="http://www.arcy.cn/" target="_blank">A+ 管理后台</a>
                </div>
            </div>
        </div>
        <!--右侧部分结束-->
    </div>
    <script src="/admin/js/jquery.min.js?v=2.1.4" type="text/javascript"></script>
    <script src="/admin/js/bootstrap.min.js?v=3.3.6" type="text/javascript"></script>
    <script src="/admin/js/plugins/metisMenu/jquery.metisMenu.js" type="text/javascript"></script>
    <script src="/admin/js/plugins/slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/admin/js/plugins/layui/layui.js" type="text/javascript"></script>
    <script src="/admin/js/hplus.min.js?v=4.1.0" type="text/javascript"></script>
    <script src="/admin/js/contabs.min.js" type="text/javascript"></script>
    <script src="/admin/js/plugins/pace/pace.min.js" type="text/javascript"></script>
</body>
</html>
