<!DOCTYPE html>
<html>
<head>

    <title>后台首页</title>
    @include('admin.layouts.style')
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
			<div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>系统信息</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <tbody>
                        @foreach ($info as $key=>$val)
                        	<tr>
                        		<td>{{ $key }}</td>
                        		<td>{{ $val }}</td>
                        	</tr>
						@endforeach
							<tr>
								<td>服务器空间使用率</td>
								<td>
									<div class="col-sm-4" style="padding: 0;">
										<h5>{{ $disk_precent }}%</h5>
										<div class="progress progress-mini">
				                            <div style="width: {{ $disk_precent }}%;" class="progress-bar"></div>
				                        </div>	
									</div>
                        		</td>
							</tr>
                        </tbody>
                    </table>

                </div>
			</div>

			<div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>开发团队</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <tbody>
                        	<tr>
                        		<td>部门名称</td>
                        		<td>{{ config('system.team_name') }}</td>
                        	</tr> 
                        	<tr>
                        		<td>开发人员</td>
                        		<td>
                        			@foreach (config('system.product_author') as $val)
                        			<a href="{{ $val['url'] }}" target="_blank">{{ $val['name'] }}</a>({{$val['email']}}) 
                        			@endforeach
                        		</td>
                        	</tr>
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
	</div>
</div>
@include('admin.layouts.script')
</body>
</html>