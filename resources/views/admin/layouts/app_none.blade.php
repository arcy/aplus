<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    @include('admin.layouts.style')

    @yield('css')

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="row">
            <div class="col-sm-12">
                @yield('main')
            </div>
        </div>
    </div>

    @include('admin.layouts.script')
    

    @yield('script')
</body>
</html>