<div class="ibox-title">
    <?php
        $menus = Admin::pageUserMenu();
    ?>
    @if(!is_empty($menus))
        @foreach($menus as $menu)<a href="{{ $menu['url'] }}" class="menu-bnt @if(Route::currentRouteName() == $menu['rule_as']) current @endif">{{ $menu['title'] }}</a>@endforeach
    @endif
    <div class="ibox-tools">
    	<a class="refresh-link" title="刷新" data-event="onrefresh">
            <i class="fa fa-refresh"></i>
        </a>
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
</div>