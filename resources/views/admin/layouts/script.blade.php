<script type="text/javascript">
var CT = {
    JS  : '/admin/js/',
    CSS : '/admin/css/',
    IMG : '/admin/img/',
    THIRD : '/admin/js/plugins/',
    URL : document.URL
};
</script>
<script type="text/javascript" src="/admin/js/jquery.min.js?v=2.1.4"></script>
<script type="text/javascript" src="/admin/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/admin/js/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/admin/js/plugins/layui/layui.js"></script>
<script type="text/javascript" src="/admin/js/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="/admin/js/content.min.js?v=1.0.0"></script>
<script type="text/javascript" src="/admin/js/atr-event.js?v=1.0.0"></script>
<script type="text/javascript" src="/admin/js/functions.js?v=1.0.0"></script>
<script type="text/javascript" type="text/javascript">
$(document).ready(function(){
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "20",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};

	@if(session('jump_success_message'))
	toastr.success('{{session("jump_success_message")}}');
	@endif

	@if(session('jump_error_message'))
	toastr.error('{{session("jump_error_message")}}');
	@endif

	$(".i-checks").iCheck({
		checkboxClass:"icheckbox_square-green",
		radioClass:"iradio_square-green"
	});
})

</script>