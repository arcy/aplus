<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">


<link rel="shortcut icon" href="favicon.ico"> 
<link href="/admin/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
<link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
<link href="/admin/css/animate.min.css" rel="stylesheet">
<link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<link href="/admin/js/plugins/layui/css/layui.css" rel="stylesheet">
<link href="/admin/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/admin/css/aplus.style.css?v=4.1.0" rel="stylesheet">