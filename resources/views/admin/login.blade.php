
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<title>世宇科技售后服务中心——后台登录</title>

<!--common css-->
<link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/admin/css/aplus.style.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->
</head>
<body class="signin-body">
<div class="container signin-container">
    <div class="text-center signin-head">
        <img src="/admin/img/logo.png" style="width: 104px;">
    </div>
    <form class="signin-form" action="{{ URL::route('admin.login') }}" method="post" data-event="onform">
        {{ csrf_field() }}
        <div class="form-group signin-form-group">
            <input datatype="s3-15" nullmsg="用户名不能为空！" errormsg="用户名介乎4到16个字符！" type="text" class="signin-form-control" name="username" id="username" placeholder="账号" autocomplete="off" />
        </div>
        <div class="form-group signin-form-group">
            <input datatype="s6-30" nullmsg="密码不能为空！" errormsg="密码介乎8到30个字符！" type="password" class="signin-form-control" name="password" id="password" placeholder="密码" autocomplete="off" />
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-login btn-block">登 陆</button>
        </div>
    </form>
    <p class="text-center text-grap"><a href="http://www.sy.cn" style="color:#7A7676;" target="_blank">中山世宇动漫科技有限公司</a> &copy; 版权所有</p>
</div>
<!--import third-party js-->
<script type="text/javascript">
CT = {
    JS  : '/admin/js/',
    CSS : '/admin/css/',
    IMG : '/admin/img/',
    THIRD : '/admin/third-party/',
    URL : document.URL
}
</script>
<style type="text/css">
    .Validform_error{
        border-bottom-color: #FF0000;
    }
</style>
<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/admin/js/bootstrap.min.js"></script>
<script src="/admin/js/atr-event.js"></script>
<script src="/admin/js/functions.js"></script>
</body>
</html>