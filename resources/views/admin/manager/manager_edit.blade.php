@extends('admin.layouts.app')
@section('css')
<link href="/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
<link href="/admin/css/plugins/webuploader/webuploader.css" rel="stylesheet">
@endsection
@section('main')
<form method="get" class="form-horizontal" data-event="onform" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">用户名</label>
        <div class="col-sm-10">
            <input type="text" value="{{ $info->username }}"  class="form-control" disabled="disabled">
            <span class="help-block m-b-none">登陆账号，介乎4-16个字符</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">用户姓名</label>
        <div class="col-sm-10">
            <input type="text" value="{{ $info->name }}" name="name" datatype="s2-16" nullmsg="用户姓名不能为空！" errormsg="用户姓名介乎2-16个字符！" class="form-control" data-trigger="hover">
            <span class="help-block m-b-none">介乎2-16个字符</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">登陆密码</label>
        <div class="col-sm-10">
            <input type="text" name="password" datatype="s8-16" nullmsg="登陆密码不能为空！" errormsg="登陆密码介乎8-16个字符！" class="form-control" data-trigger="hover" ignore="ignore">
            <span class="help-block m-b-none">密码不更改留空</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">头像</label>

        <div class="col-sm-6">
   			<input type="text" value="{{ $info->headimgurl }}" datatype="*" nullmsg="头像不能为空！" name="headimgurl" class="form-control" />
        </div>

        <div class="col-sm-4">
            <a type="button" id="headimgPicker">
                <i class="fa fa-upload"></i> 上传头像
            </a>
            <span class="help-block m-b-none" id="headimgPickerTips">
                
            </span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">是否启用</label>

        <div class="col-sm-10">
            <input type="checkbox" name="status" class="js-switch-2" @if($info->status == 1) checked @endif/>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <button class="btn btn-primary" type="submit">保存</button>
        </div>
    </div>
</form>

@endsection
@section('script')
<script type="text/javascript">
	//swich按钮
	$(function($){

		var elem2 = document.querySelector('.js-switch-2');
		var init2 = new Switchery(elem2);

        var uploader = WebUploader.create({
            //是否自动上传
            auto : true,
            // swf文件路径
            swf: CT.JS + '/plugins/webuploader/uploader.swf',

            // 文件接收服务端。
            server: '{{ URL::route("admin.attachment.upload.save") }}',

            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: {
                id: '#headimgPicker',
                multiple: false
            },

            // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            // resize: false
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            },
            fileVal: 'file',
            fileSingleSizeLimit: 200*1024,
            //可重复上传
            duplicate :true
        });

        uploader.on('uploadBeforeSend',function (object ,data ,headers){
            headers['X-CSRF-TOKEN']=  $('meta[name="csrf-token"]').attr('content');
        });

        // 文件上传过程中创建进度条实时显示。
        uploader.on( 'uploadProgress', function( file, percentage ) {
            var $li = $( '#headimgPickerTips' ),
                $percent = $li.find('#headimgprocess');

            // 避免重复创建
            if ( !$percent.length ) {
                $percent = $('<div id="headimgprocess"><h5>0%</h5><div class="progress progress-mini"><div style="width: 0%;" class="progress-bar"></div></div>');
                $li.html($percent);
            }

            $percent.find('h5').html(percentage * 100 + '%').end().find('.progress-bar').css( 'width', percentage * 100 + '%' );
        });


        // 文件上传失败，显示上传出错。
        uploader.on( 'uploadError', function( file ) {
            $( '#headimgPickerTips' ).html('上传失败，请重新上传！');
        });

        //选择时出现错误
        uploader.on('error',function(type){
            if(type == 'F_EXCEED_SIZE'){
                tips('文件不能超过200K！');
            }else if (type == 'Q_TYPE_DENIED') {
                tips('文件类型不正确！');
            }
        })

        //上传成功
        uploader.on('uploadSuccess',function(file,response){
            var $li = $( '#headimgPickerTips' );
            if(response.code == 1){
                $('input[name=headimgurl]').val(response.data.path);
            }else{
                $li.html(response.msg);
            }
        })

	})
	
</script>
<script src="/admin/js/plugins/switchery/switchery.js"></script>
<script src="/admin/js/plugins/webuploader/webuploader.min.js"></script>
@endsection