@extends('admin.layouts.app_nonav')
@section('css')
<link href="/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
<link href="/admin/css/plugins/webuploader/webuploader.css" rel="stylesheet">
@endsection
@section('main')
<form method="get" class="form-horizontal" data-event="onform" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="col-sm-2 control-label">用户ID</label>
        <div class="col-sm-10">
            <p class="form-control-static">{{ $SELF->admin_id }}</p>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">用户名</label>
        <div class="col-sm-10">
            <p class="form-control-static">{{ $SELF->username }}</p>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">旧密码</label>
        <div class="col-sm-10">
            <input type="text" name="old_pass" datatype="s8-16" nullmsg="旧密码不能为空！" errormsg="旧密码介乎8-16个字符！" class="form-control">
            <span class="help-block m-b-none">介乎8-16个字符</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">新密码</label>
        <div class="col-sm-10">
            <input type="text" name="new_pass" datatype="s8-16" nullmsg="新密码不能为空！" errormsg="新密码介乎8-16个字符！" class="form-control">
            <span class="help-block m-b-none">介乎8-16个字符</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">确认密码</label>
        <div class="col-sm-10">
            <input type="text" name="com_pass" recheck="new_pass" datatype="s8-16" nullmsg="确认密码不能为空！" errormsg="两次输入密码不一致！" class="form-control">
            <span class="help-block m-b-none">介乎8-16个字符</span>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <button class="btn btn-primary" type="submit">保存</button>
        </div>
    </div>
</form>

@endsection
@section('script')
<script type="text/javascript">
	//swich按钮
	$(function($){

		var elem2 = document.querySelector('.js-switch-2');
		var init2 = new Switchery(elem2);

        var uploader = WebUploader.create({
            //是否自动上传
            auto : true,
            // swf文件路径
            swf: CT.JS + '/plugins/webuploader/uploader.swf',

            // 文件接收服务端。
            server: '{{ URL::route("admin.attachment.upload.save") }}',

            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: {
                id: '#headimgPicker',
                multiple: false
            },

            // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            // resize: false
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            },
            fileVal: 'file',
            fileSingleSizeLimit: 200*1024,
            //可重复上传
            duplicate :true
        });

        uploader.on('uploadBeforeSend',function (object ,data ,headers){
            headers['X-CSRF-TOKEN']=  $('meta[name="csrf-token"]').attr('content');
        });

        // 文件上传过程中创建进度条实时显示。
        uploader.on( 'uploadProgress', function( file, percentage ) {
            var $li = $( '#headimgPickerTips' ),
                $percent = $li.find('#headimgprocess');

            // 避免重复创建
            if ( !$percent.length ) {
                $percent = $('<div id="headimgprocess"><h5>0%</h5><div class="progress progress-mini"><div style="width: 0%;" class="progress-bar"></div></div>');
                $li.html($percent);
            }

            $percent.find('h5').html(percentage * 100 + '%').end().find('.progress-bar').css( 'width', percentage * 100 + '%' );
        });


        // 文件上传失败，显示上传出错。
        uploader.on( 'uploadError', function( file ) {
            $( '#headimgPickerTips' ).html('上传失败，请重新上传！');
        });

        //选择时出现错误
        uploader.on('error',function(type){
            if(type == 'F_EXCEED_SIZE'){
                tips('文件不能超过200K！');
            }else if (type == 'Q_TYPE_DENIED') {
                tips('文件类型不正确！');
            }
        })

        //上传成功
        uploader.on('uploadSuccess',function(file,response){
            var $li = $( '#headimgPickerTips' );
            if(response.code == 1){
                $('input[name=headimgurl]').val(response.data.path);
            }else{
                $li.html(response.msg);
            }
        })

	})
	
</script>
<script src="/admin/js/plugins/switchery/switchery.js"></script>
<script src="/admin/js/plugins/webuploader/webuploader.min.js"></script>
@endsection