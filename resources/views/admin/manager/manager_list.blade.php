@extends('admin.layouts.app')
@section('main')
<div class="table-responsive">
    <table class="table table-bordered table-striped">
    	<thead>
            <tr>
                <th>#</th>
                <th>用户名</th>
                <th>用户昵称</th>
                <th>用户头像</th>
                <th>状态</th>
                <th>添加时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lists as $list)
            <tr>
                <td>{{ $list->admin_id }}</td>
                <td>{{ $list->username }}</td>
                <td>{{ $list->name }}</td>
                <td><img src="{{ $list->headimgurl }}" /></td>
                <td>
                    @if($list->status == 1)
                    <a href="{{ URL::route('admin.manager.changeStatus',['option'=>'status','id'=>$list->admin_id]) }}" data-event="onstatus"><i class="fa fa-check"></i></a>
                    @else
                    <a href="{{ URL::route('admin.manager.changeStatus',['option'=>'status','id'=>$list->admin_id]) }}" data-event="onstatus"><i class="fa fa-close"></i></a>
                    @endif
                </td>
                <td>{{ $list->create_at }}</td>
                <td>
                    <a href="{{ URL::route('admin.manager.edit',['id'=>$list->admin_id]) }}" class="btn btn-white btn-bitbucket btn-sm"><i class="fa fa-paste"></i> 编辑</a>
                    <a href="{{ URL::route('admin.manager.role',['id'=>$list->admin_id]) }}" class="btn btn-white btn-bitbucket btn-sm" data-title="{{$list->username}}-分配角色"><i class="fa fa-group"></i> 分配角色</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-sm-12">
        {{$lists->links()}}
    </div>
</div>
@endsection