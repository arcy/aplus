@extends('admin.layouts.app_none')
@section('main')
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>角色分配</h5>
        <div class="ibox-tools">
            <a class="refresh-link" title="刷新" data-event="onrefresh">
                <i class="fa fa-refresh"></i>
            </a>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <h1 class="title">{{$info->username}}</h1>
        <p>可以为管理员分配多个角色</p>
    </div>
</div>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>角色列表</h5>
        <div class="ibox-tools">
            <a class="refresh-link" title="刷新" data-event="onrefresh">
                <i class="fa fa-refresh"></i>
            </a>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form data-event="onform">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>角色名</th>
                        <th>描述</th>
                        <th>加入</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($role_list as $list)
                    <tr>
                        <td>{{$list->role_id}}</td>
                        <td>{{$list->name}}</td>
                        <td>{{$list->remark}}</td>
                        <td class="text-navy">  <input ignore="ignore" value="{{$list->role_id}}" type="checkbox" class="i-checks" name="role_id[]" @if(in_array($list->role_id,$role_ids)) checked="" @endif> </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="hr-line-dashed"></div>
            <button class="btn btn-primary" type="submit">保存</button>     
        </form>  
    </div>
</div>
@endsection