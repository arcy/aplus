@if ($paginator->hasPages())
    <div class="btn-group pull-right">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="btn btn-white disabled"><i class="fa fa-chevron-left"></i></a>
        @else
            <a class="btn btn-white" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fa fa-chevron-left"></i></a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                    <!-- <span>{{ $element }}</span> -->
                <a class="btn btn-white disabled">{{ $element }}</a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <!-- <li class="active"><span>{{ $page }}</span></li> -->
                        <span class="btn btn-white active">{{ $page }}</span>
                    @else
                        <a href="{{ $url }}" class="btn btn-white">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <!-- <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li> -->
            <a class="btn btn-white" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fa fa-chevron-right"></i></a>
        @else
            <!-- <li class="disabled"><span>&raquo;</span></li> -->
            <a type="button" class="btn btn-white disabled"><i class="fa fa-chevron-right"></i></a>
        @endif
    </div>
@endif
