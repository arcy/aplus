<?php

/**
 *@author arcytan
 *@date 2017-03-06
 *@description 后台路由
 */
Route::group(['namespace'=>'Admin','prefix'=>'admin' , 'as'=>'admin.'] ,function(){
	
	/* 登录页 */
	Route::match(['get','post'], '/' ,['uses'=>'LoginController@login']);
	Route::match(['get','post'], 'login' ,['uses'=>'LoginController@login','as'=>'login']);
	Route::get('logout' , ['uses'=>'LoginController@logout', 'as'=>'logout']);
	Route::get('mkpsd' , ['uses'=>'LoginController@mkpsd']);

	/* 后台首页 */
	// Route::get('/', 'HomeController@home');
	Route::get('home', ['uses'=>'HomeController@home', 'as'=>'home']);
	Route::get('index', ['uses'=>'HomeController@index', 'as'=>'home.index']);

	/* 权限路由 */
	Route::group(['namespace'=>'Auth', 'prefix'=>'auth', 'as'=>'auth.'], function(){
		Route::get('rule/list', ['uses'=>'RuleController@lists', 'as'=>'rule.list']);
		Route::match(['get','post'],'rule/add', ['uses'=>'RuleController@add', 'as'=>'rule.add']);
		Route::match(['get','post'],'rule/edit',['uses'=>'RuleController@edit', 'as'=>'rule.edit']);
		Route::post('rule/changeStatus',['uses'=>'RuleController@changeStatus', 'as'=>'rule.changeStatus']);
		Route::post('rule/del',['uses'=>'RuleController@del', 'as'=>'rule.del']);

		//角色路由
		Route::get('role/list',['uses'=>'RoleController@lists', 'as'=>'role.list']);
		Route::match(['get','post'],'role/add', ['uses'=>'RoleController@add', 'as'=>'role.add']);
		Route::match(['get','post'],'role/edit',['uses'=>'RoleController@edit', 'as'=>'role.edit']);
		Route::post('role/changeStatus',['uses'=>'RoleController@changeStatus', 'as'=>'role.changeStatus']);
		Route::post('role/del',['uses'=>'RoleController@del', 'as'=>'role.del']);
	});


	/* 管理员管理路由 */
	// Route::get('manager','ManagerController@lists');
	Route::get('manager/list',['uses'=>'ManagerController@lists','as'=>'manager.list']);
	Route::match(['get','post'],'manager/add',['uses'=>'ManagerController@add','as'=>'manager.add']);
	Route::match(['get','post'],'manager/edit',['uses'=>'ManagerController@edit','as'=>'manager.edit']);
	Route::post('manager/changeStatus',['uses'=>'ManagerController@changeStatus', 'as'=>'manager.changeStatus']);
	Route::post('manager/del',['uses'=>'ManagerController@del', 'as'=>'manager.del']);
	//修改管理员个人信息
	Route::match(['get','post'],'manager/editProfile',['uses'=>'ManagerController@editProfile','as'=>'manager.editProfile']); 
	//修改管理员密码
	Route::match(['get','post'],'manager/editPassword',['uses'=>'ManagerController@editPassword','as'=>'manager.editPassword']);
	//为管理员分配角色 
	Route::match(['get','post'],'manager/role',['uses'=>'ManagerController@role','as'=>'manager.role']); 
	
	//附件路由
	Route::group(['namespace'=>'Attachment', 'prefix'=>'attachment', 'as'=>'attachment.'], function(){
		Route::post('upload/save',['uses'=>'UploadController@save','as'=>'upload.save']);
	});
});